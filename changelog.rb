require 'json'
changelog = IO.readlines("../../dev/sysward/CHANGELOG.md")

article = {}

changelog.each_with_index do |line, index|
  if line.count("#") == 1
    if article.keys.count == 5
      puts "writing article"
      puts article

      file =<<FILE
---
title: "#{article[:title]}"
date: #{article[:date]}
categories: #{article[:tags].to_json}
---

#{article[:body].map(&:strip).join("\n").strip}
FILE

      puts file
      File.open("content/post/#{article[:url]}.md", "w+") { |f| f.write(file) }
    end
    title = line.split("# ")
    article[:title] = "Changelog: #{title[1].strip}"
    article[:url] = article[:title].gsub(":", "-").gsub(" ", "").downcase
    article[:date] = "#{title[1].strip}T12:00:00-04:00"
    article[:tags] = ["changelog"]
    article[:body] = []
  end

  if line.count("##") == 2 && line.include?("Agent")
    article[:tags] << "agent"
  end

  if line.count("##") == 2 && line.include?("Platform")
    article[:tags] << "platform"
  end

  if line.count("#") == 0
    article[:body] << line.strip
  end
end
