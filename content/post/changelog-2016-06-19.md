---
title: "Changelog: 2016-06-19"
date: 2016-06-19T12:00:00-04:00
categories: ["changelog","platform"]
---

* Users can now disable or enable email alerts (enabled by default)
