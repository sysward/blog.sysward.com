---
title: "Changelog: 2017-07-06"
date: 2017-07-06T12:00:00-04:00
categories: ["changelog","platform"]
---

* Updated CVE listing pages
* Added CVE counts to agent listing page to provide more detail on individual agents
* Added CVE tab to agents detail page
