---
title: "Changelog: 2015-09-27"
date: 2015-09-27T12:00:00-04:00
categories: ["changelog","platform","agent"]
---

* Performance improvements on agent checkin (before: 150ms average, after: 5ms)


* Bug fix that was causing agent to hang on spotty connections and never finish running
* OpenSUSE and SUSE are now supported operating systems
