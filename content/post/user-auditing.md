---
title: "User Auditing"
date: 2015-04-23T23:18:12-04:00
---

Organization admins can now view an audit history for users.

<img src="/images/audit1.png" width="75%" />

Example audit page:

<img src="/images/audit2.png" width="75%" />
