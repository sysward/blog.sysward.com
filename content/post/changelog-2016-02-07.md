---
title: "Changelog: 2016-02-07"
date: 2016-02-07T12:00:00-04:00
categories: ["changelog","platform","agent"]
---

* Add support for custom hostname being filtered on


* Fix a bug with Ubuntu agent not detecting certain distro names properly
* Add `-hostname` and `-custom-hostname` flags to set them in the dashboard
