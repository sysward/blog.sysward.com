---
title: "New Design"
date: 2015-05-17T23:18:16-04:00
---
We’ve redesigned the dashboard to make everything easier to get to and more easily understood with large amounts of servers.

<img src="/images/newdesign.png" width="75%" />
