---
title: "Changelog: 2014-12-23"
date: 2014-12-23T12:00:00-04:00
categories: ["changelog","platform"]
---

* Performance improvements to the agent API
* Performance improvements to the dashboard
