---
title: "Changelog: 2014-07-23"
date: 2014-07-23T12:00:00-04:00
categories: ["changelog","platform"]
---

* Display manually upgraded packages (user logged into server and upgraded something)
* Better failure reporting - email alerts on job failures
