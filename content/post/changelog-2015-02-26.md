---
title: "Changelog: 2015-02-26"
date: 2015-02-26T12:00:00-04:00
categories: ["changelog","platform"]
---

* New feature: Packages page gives package overview for all systems
