---
title: "Changelog: 2015-02-09"
date: 2015-02-09T12:00:00-04:00
categories: ["changelog","platform"]
---

* Fix display issues with Ubuntu/CentOS icons on dashboard
* Fix ordering on pricing page
