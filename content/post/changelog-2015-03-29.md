---
title: "Changelog: 2015-03-29"
date: 2015-03-29T12:00:00-04:00
categories: ["changelog","platform"]
---

* Add support for group, os, and checkin sorting
* Custom hostnames can now be set by just clicking on the agent hostname
