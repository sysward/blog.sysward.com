---
title: "Changelog: 2017-05-05"
date: 2017-05-05T12:00:00-04:00
categories: ["changelog","platform"]
---

* Updated CVE details pages
* Improved performance on CVE detection
