---
title: "Agent Proxy Support"
date: 2015-03-20T23:17:53-04:00
---

You can now set HTTPS_PROXY environment parameter to use a secure proxy.

Example to send everything through a http and https proxy (proxy server used here is mitmproxy):

``` shell
http_proxy=http://192.168.5.100:8080/ HTTPS_PROXY=http://192.168.5.100:8080/ ./sysward
```

lower case http_proxy must be used for apt to pick it up (HTTP_PROXY) does not work. If you need proxy support we suggest putting these at the top of your crontab file where other environment variables are stored.
