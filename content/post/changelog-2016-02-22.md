---
title: "Changelog: 2016-02-22"
date: 2016-02-22T12:00:00-04:00
categories: ["changelog","platform"]
---

* Ability to select individual packages for installation/upgrade
