---
title: "Changelog: 2017-07-29"
date: 2017-07-29T12:00:00-04:00
categories: ["changelog","platform"]
---

* Added an upgrade button to apply CVE patches
