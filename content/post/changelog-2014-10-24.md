---
title: "Changelog: 2014-10-24"
date: 2014-10-24T12:00:00-04:00
categories: ["changelog","platform","agent"]
---

* Users can now set their timezone
* Agent list can now be exported as a CSV
* Added sorting to security/regular update counts on agent list
* CentOS/RHEL are now both supported


* CentOS/RHEL are now both supported
