---
title: "Changelog: 2015-11-07"
date: 2015-11-07T12:00:00-04:00
categories: ["changelog","platform"]
---

* New feature: CVE alerts now show in the dashboard when vulnerable software is detected by SysWard
