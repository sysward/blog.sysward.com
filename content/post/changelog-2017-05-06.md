---
title: "Changelog: 2017-05-06"
date: 2017-05-06T12:00:00-04:00
categories: ["changelog","agent"]
---

* Convert agent builds to use Glide instead of Godep
* Update to Go 1.8
