---
title: "Changelog: 2017-05-07"
date: 2017-05-07T12:00:00-04:00
categories: ["changelog","agent"]
---

* Fix CentOS and Debian version information reported by agent
