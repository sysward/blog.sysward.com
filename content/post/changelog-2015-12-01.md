---
title: "Changelog: 2015-12-01"
date: 2015-12-01T12:00:00-04:00
categories: ["changelog","agent"]
---

* Fix a bug with Debian based systems and counting security related packages
