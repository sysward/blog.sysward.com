---
title: "New Feature Released: Package Overview"
date: 2014-08-08T23:00:21-04:00
---
We’ve released a new feature today that allows you to manage your updates from a different perspective.


In addition to individual agent views, group views, there is now also a package view available. The package view lists all packages available for upgrading, shows which hosts and groups would be effected by applying them, and gives you the ability to upgrade them.
