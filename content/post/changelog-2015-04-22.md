---
title: "Changelog: 2015-04-22"
date: 2015-04-22T12:00:00-04:00
categories: ["changelog","platform"]
---

* New feature: User auditing released
