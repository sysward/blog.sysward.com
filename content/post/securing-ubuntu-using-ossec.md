---
title: "Securing Ubuntu Using Ossec"
date: 2014-08-09T23:17:27-04:00
---
We'll be covering how to install a HIDS (Host-Intrusion Detection System) called OSSEC in this tutorial on a client and a server.

We're going to start off with a `Vagrantfile` so we can easily test this without damaging any live servers.
A list of available boxes is over at http://www.vagrantbox.es/ . A nice tip is that you can specify the URL as the box option in your `Vagrantfile`
that way people who use your `Vagrantfile` won't have to import the machine first (Vagrant only downloads it once).

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |con|

con.vm.define :ossec_server do |config|
config.vm.host_name = "ossec-server"
config.vm.box = 'https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box'
config.vm.network :private_network, ip: "10.10.10.100"
config.vm.network :forwarded_port, guest: 22, host: 3200, auto: true
end

con.vm.define :ossec_client do |config|
config.vm.host_name = "ossec-client"
config.vm.box = 'https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box'
config.vm.network :private_network, ip: "10.10.10.101"
config.vm.network :forwarded_port, guest: 22, host: 3201, auto: true
end

end
```

Another tip while testing with this type of setup (server/client) is to use tmux so you can vertically split the screens and switch between them easily. You can open tmux using `tmux` and then split using `Ctrl+B + %` and then `Ctrl+B o` to switch between them. You can then `vagrant ssh ossec_server` in one pane and then `vagrant ssh ossec_client` in the other.

Prep on both hosts:
``` bash
apt-get install build-essential
```

Download OSSEC 2.8 on both and extract it:
``` bash
curl -O http://www.ossec.net/files/ossec-hids-2.8.tar.gz
tar xzvf ossec-hids-2.8.tar.gz
```

Now run these commands on each machine, we'll start off configuring the server first and then move on to configuring the first OSSEC client.

``` bash
cd ossec-hids-2.8
./install.sh
```

Choose `en` at the prompt and hit enter to continue.

On the agent, choose `agent` at the next prompt. On the server, choose `server`.

## Server Configuration
Install at `/var/ossec`

`Do you want e-mail notification? [y]`: If you want to receive emails, enter your email and hit enter to continue. OSSec will discover your SMTP server - hit enter at the next prompt if the server information is correct.

`Do you want to install the integrity check daemon? [y]`: We want to enable this so changes to system files are monitored.

`Do you want to run the rootkit detection engine? [y]`: We also want this enabled. Rootkits can hide themselves from normal investigations so if an attacker is attempting to install one, we need to know.

`Do you want to enable active response? [y]`: OSSEC comes with a nice feature that can automatically block invasive behaviors. Enabling this can lock you out of your machine, so be careful. We will cover how to add yourself to a whitelist later so you can ensure access.

`Do you want to enable the firewall drop policy? [y]`: Yes we want this because it will help prevent certain low-hanging fruit attacks such as SSH bruteforcing and port scanning.

Next the installer prompts you to add IPs to the whitelist - it defaults to the IP your logging in on. If you need to, add more.

`Do you want to enable remote syslog? [y]:` We want to allow this to let the OSSEC server to receive alerts from agents.

Configuration is done now. Once you hit enter it will configure the server and display this message:

``` text
- System is Debian (Ubuntu or derivative).
- Init script modified to start OSSEC HIDS during boot.

- Configuration finished properly.

- To start OSSEC HIDS:
/var/ossec/bin/ossec-control start

- To stop OSSEC HIDS:
/var/ossec/bin/ossec-control stop

- The configuration can be viewed or modified at /var/ossec/etc/ossec.conf

Thanks for using the OSSEC HIDS.
If you have any question, suggestion or if you find any bug,
contact us at contact@ossec.net or using our public maillist at
ossec-list@ossec.net
( http://www.ossec.net/main/support/ ).

More information can be found at http://www.ossec.net

--- Press ENTER to finish (maybe more information below). ---
```

We're going to need to use `/var/ossec/bin/manage_agents` later to add agents.

## Client Configuration

Lets setup the agent now:

`What's the IP Address or hostname of the OSSEC HIDS server?`: 10.10.10.100 (if using our Vagrantfile or use your own IP)

`Do you want to install the integrity check daemon? [y]`: We want to enable this so changes to system files are monitored.

`Do you want to run the rootkit detection engine? [y]`: We also want this enabled. Rootkits can hide themselves from normal investigations so if an attacker is attempting to install one, we need to know.

`Do you want to enable active response? [y]`: OSSEC comes with a nice feature that can automatically block invasive behaviors. Enabling this can lock you out of your machine, so be careful. We will cover how to add yourself to a whitelist later so you can ensure access.

That's the final configuration step (and it should look familiar to the server setup) - now the installer will finish.

Now you'll see the end screen:

``` text
- System is Debian (Ubuntu or derivative).
- Init script modified to start OSSEC HIDS during boot.

- Configuration finished properly.

- To start OSSEC HIDS:
/var/ossec/bin/ossec-control start

- To stop OSSEC HIDS:
/var/ossec/bin/ossec-control stop

- The configuration can be viewed or modified at /var/ossec/etc/ossec.conf

Thanks for using the OSSEC HIDS.
If you have ny question, suggestion or if you find any bug,
contact us at contact@ossec.net or using our public maillist at
ossec-list@ossec.net
( http://www.ossec.net/main/support/ ).

More information can be found at http://www.ossec.net

--- Press ENTER to finish (maybe more information below). ---
```

## Connecting the OSSEC client and server
We now have an agent and a server configured. Lets head over to the server and add our agent now:

Run the manage agents command:
``
/var/ossec/bin/manage_agents
``

You'll be prompted on what actions you want to perform, since this is our first time running this, lets choose `(A)` to Add an Agent.

We'll name it `client1`.

The IP (if using our Vagrantfile) is `10.10.10.101`, we'll leave the ID as the default 001 and hit `y` at the final confirmation.

Hit `E` to display the key and then type in `001` (the ID of the agent we just made) and copy this key for later.

Hit `Q` to quit and and then we'll restart OSSEC:

``
/etc/init.d/ossec restart
``

Now on the agent, lets sync our keys over:
``
/var/ossec/bin/manage_agents
``

We'll choose `(I)` and then paste in our key we copied earlier, hit enter and then confirm the change and hit Q to quit.

Now restart the agent service:
``
/etc/init.d/ossec restart
``

Now we can check to make sure its sending by opening `/var/ossec/logs/ossec.log` on the agent:
``` text
...
2014/08/10 14:29:30 ossec-rootcheck: INFO: Started (pid: 10806).
2014/08/10 14:29:30 ossec-syscheckd: INFO: Monitoring directory: '/etc'.
2014/08/10 14:29:30 ossec-syscheckd: INFO: Monitoring directory: '/usr/bin'.
2014/08/10 14:29:30 ossec-syscheckd: INFO: Monitoring directory: '/usr/sbin'.
2014/08/10 14:29:30 ossec-syscheckd: INFO: Monitoring directory: '/bin'.
2014/08/10 14:29:30 ossec-syscheckd: INFO: Monitoring directory: '/sbin'.
2014/08/10 14:29:32 ossec-logcollector(1950): INFO: Analyzing file: '/var/log/auth.log'.
2014/08/10 14:29:32 ossec-logcollector(1950): INFO: Analyzing file: '/var/log/syslog'.
2014/08/10 14:29:32 ossec-logcollector(1950): INFO: Analyzing file: '/var/log/dpkg.log'.
2014/08/10 14:29:32 ossec-logcollector: INFO: Monitoring output of command(360): df -h
2014/08/10 14:29:32 ossec-logcollector: INFO: Monitoring full output of command(360): netstat -tan |grep LISTEN |grep -v 127.0.0.1 | sort
2014/08/10 14:29:32 ossec-logcollector: INFO: Monitoring full output of command(360): last -n 5
2014/08/10 14:29:32 ossec-logcollector: INFO: Started (pid: 10803).
2014/08/10 14:30:32 ossec-syscheckd: INFO: Starting syscheck scan (forwarding database).
2014/08/10 14:30:32 ossec-syscheckd: INFO: Starting syscheck database (pre-scan).
2014/08/10 14:36:51 ossec-syscheckd: INFO: Finished creating syscheck database (pre-scan completed).
2014/08/10 14:37:03 ossec-syscheckd: INFO: Ending syscheck scan (forwarding database).
2014/08/10 14:37:23 ossec-rootcheck: INFO: Starting rootcheck scan.
2014/08/10 14:40:30 ossec-rootcheck: INFO: Ending rootcheck scan.
...
```

And on the server we can check our alerts in `/var/ossec/logs/alerts/alerts.log`:
``` text
...
** Alert 1407681444.3594: mail - ossec,rootcheck,
2014 Aug 10 14:37:24 (client1) 10.10.10.101->rootcheck
Rule: 510 (level 7) -> 'Host-based anomaly detection event (rootcheck).'
File '/dev/.blkid.tab.old' present on /dev. Possible hidden file.
...
```

*Note:* Depending on whether or not your machine is able to send email, you may have errors in your logfile at `/var/ossec/logs/ossec.log` about being unable to connect to your mail server.

That's it! You now have a default setup of OSSEC running. If you need to change any configuration options (or made any mistakes) you can re-run the installer safely to update them.

##### Are you interested in keep your Ubuntu sytems securely patched?

Head on over to SysWard and try out our patch management system for free!
