---
title: "Changelog: 2015-05-17"
date: 2015-05-17T12:00:00-04:00
categories: ["changelog","platform","agent"]
---

* New design released with UI and overview improvements
* Custom hostnames can now be set for display within the UI
* User auditing to track who has done what in the control panel


* Support for more operating systems, now including: CentOS 6 & 7, Ubuntu 12 & 14, Debian 6 & 7
* HTTP and HTTPS Proxy Support ( read more: http://blog.sysward.com/2015/03/agent-proxy-support/ )
