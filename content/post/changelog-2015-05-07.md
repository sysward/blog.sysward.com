---
title: "Changelog: 2015-05-07"
date: 2015-05-07T12:00:00-04:00
categories: ["changelog","platform"]
---

* Fix small spelling error
* Add loading indicator to packages page
