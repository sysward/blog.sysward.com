---
title: "Changelog: 2015-12-12"
date: 2015-12-12T12:00:00-04:00
categories: ["changelog","agent"]
---

* Add the ability to tag an agent at runtime with `-group` flag
