---
title: "Changelog: 2016-03-12"
date: 2016-03-12T12:00:00-04:00
categories: ["changelog","platform"]
---

* Fixed a bug on the held/installed packages page where holding/unholding wouldn't work in certain browsers
* Sub pages inside the agent view are now directly linkable
