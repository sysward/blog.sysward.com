---
title: "Changelog: 2014-09-29"
date: 2014-09-29T12:00:00-04:00
categories: ["changelog","platform"]
---

* Jobs can now be cancelled before they run
