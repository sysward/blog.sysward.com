---
title: "Changelog: 2017-07-19"
date: 2017-07-19T12:00:00-04:00
categories: ["changelog","platform"]
---

* Updated support and contact forms to use new Zendesk and Zoho forms
