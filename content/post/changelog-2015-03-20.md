---
title: "Changelog: 2015-03-20"
date: 2015-03-20T12:00:00-04:00
categories: ["changelog","agent"]
---

* Agent now supports HTTPS proxying
