---
title: "Changelog: 2014-07-11"
date: 2014-07-11T12:00:00-04:00
categories: ["changelog","platform"]
---

* Added ability to setup 2 Factor Auth for all users
* Fix pending update counts
