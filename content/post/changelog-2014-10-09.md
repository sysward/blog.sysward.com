---
title: "Changelog: 2014-10-09"
date: 2014-10-09T12:00:00-04:00
categories: ["changelog","platform"]
---

* Organization owners can now remove users from their org
