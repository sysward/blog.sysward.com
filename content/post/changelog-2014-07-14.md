---
title: "Changelog: 2014-07-14"
date: 2014-07-14T12:00:00-04:00
categories: ["changelog","platform"]
---

* Ability to only apply security updates
* Various backend enhancements
