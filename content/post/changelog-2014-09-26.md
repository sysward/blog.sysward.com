---
title: "Changelog: 2014-09-26"
date: 2014-09-26T12:00:00-04:00
categories: ["changelog","platform"]
---

* New feature called Organizations allow you to invite multiple people to your account under a organization name.
* Cleanedup display of IPv4/IPv6 on agent lists
