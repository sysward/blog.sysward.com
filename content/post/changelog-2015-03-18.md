---
title: "Changelog: 2015-03-18"
date: 2015-03-18T12:00:00-04:00
categories: ["changelog","platform"]
---

* Fix UI bug where scrollbar would show up when no content was scrollable
* Adjust 'dead agent' timeout to 10 minutes
* Agent history is now searchable
