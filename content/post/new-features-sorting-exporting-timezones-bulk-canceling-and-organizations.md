---
title: "New Features Sorting Exporting Timezones Bulk Canceling And Organizations"
date: 2015-01-21T23:17:46-04:00
---

## Sorting
![](/images/feature_list/sorting.png)

You can now sort your agent list by pending regular or pending security updates.

## Exporting
![](/images/feature_list/csv.png)

The agent list can be exported to a CSV, which will include hostname, group, update counts, IP addresses, CPU information, memory, version, and the last time it checked in.

## Timezones
![](/images/feature_list/timezone.png)

Users can now set their timezone to get reporting data relative to where they are now.

## Bulk Cancelling
![](/images/feature_list/bulk_cancel.png)

If you queue up a bunch of jobs and need to cancel them, you can now click "Cancel All" instead of individually clicking each one.

## Organizations
![](/images/feature_list/orgs.png)

This was a big feature released a few weeks ago. You can now invite users to your organization so they can access the UI. Users are sent an email with a temporary password to login with. There is no limit on the amount of users your organization can invite.

##### Are you interested in keep your Ubuntu sytems securely patched?

Head on over to SysWard and try out our patch management system for free!
