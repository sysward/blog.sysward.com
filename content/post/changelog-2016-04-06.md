---
title: "Changelog: 2016-04-06"
date: 2016-04-06T12:00:00-04:00
categories: ["changelog","platform"]
---

* Users can now sort by package name and priority under pending updates
