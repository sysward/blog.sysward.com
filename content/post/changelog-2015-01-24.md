---
title: "Changelog: 2015-01-24"
date: 2015-01-24T12:00:00-04:00
categories: ["changelog","platform"]
---

* PayPal is now a supported payment method
