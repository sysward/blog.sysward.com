---
title: "Custom Hostnames And Sorting By Os Last Check In Group"
date: 2015-03-29T23:18:04-04:00
---

You can now sort by the Last checkin, group, and OS columns on the agent listing.

You can also now add custom hostnames.

To do so simply click anywhere in the hostname row/column for that server, enter a hostname in the form, and hit enter. The original hostname from the OS will still appear below in small text, but the main (larger linked name) will be your custom hostname. To clear it out, simply repeat these steps and clear out the input form.

<img src="/images/custom-hostnames.gif" />
