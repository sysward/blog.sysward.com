---
title: "Changelog: 2017-05-14"
date: 2017-05-14T12:00:00-04:00
categories: ["changelog","agent"]
---

* Add the ability to unregister the agent from the command line `-unregister`
