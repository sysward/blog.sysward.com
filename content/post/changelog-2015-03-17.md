---
title: "Changelog: 2015-03-17"
date: 2015-03-17T12:00:00-04:00
categories: ["changelog","platform"]
---

* UI improvements and better failed jobs page
* Preserve ordering and sorting when upgrading a package
