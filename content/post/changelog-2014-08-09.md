---
title: "Changelog: 2014-08-09"
date: 2014-08-09T12:00:00-04:00
categories: ["changelog","platform"]
---

* Improvements to the package view page for agents/servers
* Fix small grouping issue on packages
