---
title: "Changelog: 2015-03-30"
date: 2015-03-30T12:00:00-04:00
categories: ["changelog","platform"]
---

* Improvements to auditing to log when packages are held
